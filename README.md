﻿##Demo
Link: http://i392329.hera.fhict.nl/python-ml/

##Description
This was a short individual project, part of my "Open Innovation" specialization where I used some machine learning techinics, such as linear regression and logistic regression to make a prediction based on the model. The dataset was small, however real and was provided to me by a friend, who tracked his weight, calories and training days for several months. 

##Technologies

- Python 2
	* numpy
	* pandas
	* matplotlib
	* statsmodels
	* seaborn 